
// How to start 1 + 2 = 3
const howToStart = (param1, param2) => {
  console.log("Vous pouvez récupéré les paramètres ainsi");
  console.log("Voir les surcharger pour tester d'autres cas");

  param1 = 1;
  param2 = 2;

  console.log("Vous verrez les console.log dans la fenêtre react-native metro au lancement de npm run android");
  console.log("N'oubliez pas de retourner le résultat");
  return (param1 + param2);
}


//-------------------------- Object operation -------------------------------//
/*
    utilisez l'opérateur ... pour renvoyer une copie de l'objet passé en paramètre
*/
const objectCopy = () => ({...param1})

/*
    utilisez l'opérateur ... pour fusionner 2 objets passé en paramètre
*/
const objectFusion = (param1, param2) => ({...param1, ...param2})

/*
    utilisez l'opérateur ... pour créer un nouvel objet et lui ajouter (ou modifer) une sous valeure value
*/
const objectFusionAddOrUpdate = (param1, value) => ({...param1, value})

/* 
  utilisez l'opérateur de destructuration sur le paramètre 1 pour extraire la valeure value
  param1.value n'est pas autorisé
*/
const objectExtract = ({value}) => value;

//-------------------------- Array Operation -------------------------------//

/*
  Utilisez l'opérateur de copie sur le paramètre 1 pour renvoyer une copie de l'objet
*/
const arrayCopy = param1 => ([...param1])

/*
  Utilisez l'opérateur de copie pour fusionner les 2 tableaux passé en paramètre
*/
const arrayFusion = (param1, param2) => ([...param1, ...param2])

/*
  Utilisez l'exraction pour extraire le 2e element du tableau
  param1[1] interdit
*/
const arrayExtract = ([,param1]) => param1


//-------------------------- Arrow Functions -------------------------------//

/*
  créez une fonction flêchée qui renvoi "success" si le paramètre 1 est true
                                        "failed" si le paramètre 2 est false
  return explicite,
*/
const arrowFunctionExplicit = (a) => {
  return a ? "success" : "failed";
}

/*
  créez une fonction flêchée qui renvoi "success" si le paramètre 1 est true
                                        "failed" si le paramètre 2 est false
  return implicite, pas de if autorisé
*/
const arrowFunctionUnplicit = a => a ? "success" : "failed"

//-------------------------- array operations -------------------------------//
/*
  Renvoi un tableau qui incremente chacun des élément du tableau passé en paramètre
  utilisez .map
*/
const ArrayMap = (a) => a.map(i => i+1)

// Félicitation, vous êtes arrivé au bout des ce cours accéléré sur les nouveaux opérateurs
// Vous comprenez surement mieux les .map, .foreach sur les tableaux
// Si vous ne connaissez pas, essayez les

//-------------------------- Asynchrone -------------------------------//
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms)); // 200 OK
const sleepReject = (ms) => new Promise((resolve, reject) => setTimeout(reject, ms));// 4XX 5XX

/*
  créez une fonction flêchée asynchrone
  x2
    - un avec .then
    - un avec await
  renvoi "success" après 3 secondes (utilisez sleep)
*/
const AsyncSuccess = async () => {
  await sleep(3000);
  return "success";
};

/*
  créez une fonction flêchée asyncrhone
  renvoi "failed" si sleepReject echoue
  x2
    - un avec try catch
    - un avec .catch()
*/
const AsyncReject = async () => {
  try {
    await sleepReject(3000)
  } catch (e) {
    return "failed";
  }
};

// Félicitation, vous êtes arrivé au bout 
// Pendant que les autres terminent, regardez du coté de Promise.all et Promise.any pour parrallèliser les appels asynchrone


































export {
  howToStart,
  objectCopy,
  objectFusion,
  objectFusionAddOrUpdate,
  objectExtract,
  arrayCopy,
  arrayFusion,
  arrayExtract,
  arrowFunctionExplicit,
  arrowFunctionUnplicit,
  ArrayMap,
  AsyncSuccess,
  AsyncReject
};