import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ClientList from "./ClientList";
import ClientEdit from "./ClientEdit"
import { Provider } from "./clientContext";

const Tab = createStackNavigator();

const Erp = () => {
  return (
    <Provider>
      <Tab.Navigator initialRouteName="ClientList" >{/* Provider */}
        <Tab.Screen name="ClientList" component={ClientList} />
        <Tab.Screen name="ClientEdit" component={ClientEdit} />
      </Tab.Navigator>
    </Provider>
  );
}

export default Erp;