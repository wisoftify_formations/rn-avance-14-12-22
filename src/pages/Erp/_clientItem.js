import React from "react";
import { TouchableOpacity, Text,StyleSheet } from "react-native";
import { useNavigation } from '@react-navigation/native';
import {useTheme} from "@react-navigation/native";

const ClientItem = ({client}) => {
  const theme = useTheme();
  const navigation = useNavigation();

  console.log(client.id);

  return (
    <TouchableOpacity key={client.id} style={styles(theme).clientContainer} onPress={() => navigation.navigate("ClientEdit", {id: client.id})}>
        <Text style={styles(theme).clientName}>{client.name} {client.surname}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create(theme => ({
  clientContainer: {
    borderWidth: 1,
    borderColor: theme.colors.border,
    paddingVertical: 20,
    paddingHorizontal: 10
  },
  clientName: {
    fontSize: 24,
    color: theme.colors.text
  }
}))

export default ClientItem;