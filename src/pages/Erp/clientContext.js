import React, {createContext, useReducer, useContext, useEffect, useCallback} from "react";
import axios from "axios";

const ClientContext = createContext();

const Default = {
  loading: true,
  clients: []
}

function reducer(state, action) {
  switch(action.type) {
    case "SET_LOADING" : return {...state, loading: true};
    case "SET_CLIENTS" : return {...state, clients: action.payload, loading: false};
    case "SET_CLIENT"  : return {
      ...state,
      clients: state.clients.map(client => client.id === action.payload.id ? action.payload : client)
    }
    case "ADD_CLIENT"  : return {...state, clients: [...state.clients, action.payload]}
    case "DELETE_CLIENT" : return {
      ...state,
      clients: state.clients.filter(client => client.id !== action.payload)
    }
    default: return state;
  }
}

const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, Default);

  const refreshClients = async () => {
    let res = await axios.get("http://10.0.2.2:3001/clients");
    dispatch({type: "SET_CLIENTS", payload: res.data});
  }
  const updateClient = async (id, client) => {
    await axios.put(`http://10.0.2.2:3001/clients/${id}`, client);
    dispatch({type: "SET_CLIENT", id, payload: client})
  }
  const createClient = async (client) => {
    await axios.post("http://10.0.2.2:3001/clients", client);
    dispatch({type: "ADD_CLIENT", payload: client})
  }
  const deleteClient = async (id) => {
    await axios.delete(`http://10.0.2.2:3001/clients/${id}`);
    dispatch({type: "DELETE_CLIENT", id})
  }

  useEffect(() => {refreshClients()}, [])

  return (
    <ClientContext.Provider value={[{...state, refreshClients, updateClient, createClient, deleteClient}, dispatch]}>
      {children}
    </ClientContext.Provider>
  )
}

const useClientContext = () => useContext(ClientContext);

export default useClientContext;
export {Provider};