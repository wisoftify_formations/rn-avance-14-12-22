import React from 'react';
import {ActivityIndicator, ScrollView, View, RefreshControl} from "react-native";
import { useNavigation } from '@react-navigation/native';

import useClientContext from './clientContext';
import Fab from '../../uikit/Fab';
import ClientItem from "./_clientItem";

const ClientList = () => {
  const navigation = useNavigation();
  const [state] = useClientContext();
  const {clients, loading} = state;

  return (
    <View style={{flex: 1}}>
      <ScrollView style={{flex: 1}} refreshControl={
        <RefreshControl refreshing={loading} onRefresh={state.refreshClients} />
      }>
        {loading ? <ActivityIndicator /> : 
          <>
            {clients.map((client) => (
              <React.Fragment key={client.id}>
                <ClientItem client={client} />
              </React.Fragment>
            ))}
          </>
        }
      </ScrollView>
      <Fab onPress={() => navigation.navigate("ClientEdit")} />
    </View>
  );
}

export default ClientList;