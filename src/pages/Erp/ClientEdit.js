import React, {useEffect, useReducer, useCallback} from "react";
import {StyleSheet, View, Text, Alert} from "react-native";
import {Button, TextInput} from "../../uikit";
import { useNavigation, useRoute } from '@react-navigation/native';
import {useTheme} from "@react-navigation/native";

import useClientContext from "./clientContext";

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_CLIENT": return {...action.payload};
    case "SET_NAME": return {...state, name: action.payload};
    case "SET_SURNAME": return {...state, surname: action.payload};
    default: return state;
  }
}

const ClientEdit = () => {
  const theme = useTheme();
  const navigation = useNavigation();
  const {params} = useRoute();
  const {id} = params ?? {};
  const [clientsState] = useClientContext();
  const [formState, formDispatch] = useReducer(reducer, {name: "", surname: ""});
  const {name, surname} = formState;

  useEffect(() => {
    if (id)
      formDispatch({type: "SET_CLIENT", payload: clientsState.clients.find(client => client.id === id)});
  }, [id])

  useEffect(() => {
    navigation.setOptions({title: id ? "Modifier un client" : "Créer un client"});
  }, [id])

  const _handleSubmit = async () => {
    try {
      let _client = {...formState};
      if (id) {
        await clientsState.updateClient(id, _client);
        Alert.alert("Client modifié");
      } else {
        await clientsState.createClient(_client);
        Alert.alert("Client créé");
      }
      navigation.navigate("ClientList");
    } catch (e) {
      console.error(e);
      Alert.alert("Une erreur est survenue");
    }
  }

  const _handleDelete = async () => {
    try {
      await clientsState.deleteClient(id);
      Alert.alert("Client supprimé");
      navigation.navigate("ClientList");
    } catch (e) {
      console.error(e);
      Alert.alert("Une erreur est survenue");
    }
  }

  return (
    <View style={styles(theme).container}>
      <View>
        <View style={styles(theme).formGroup}>
          <Text style={styles(theme).label}>Nom :</Text>
          <TextInput
            value={name}
            placeholder="Nom..."
            onChangeText={e => formDispatch({type: "SET_NAME", payload: e})}
          />
        </View>
        <View style={styles(theme).formGroup}>
          <Text style={styles(theme).label}>Prénom :</Text>
          <TextInput
            value={surname}
            placeholder="Prénom..."
            onChangeText={e => formDispatch({type: "SET_SURNAME", payload: e})}
          />
        </View>
      </View>

      <View style={styles(theme).actions}>
        <Button title="Modifier" onPress={_handleSubmit} style={{flex: 1}} />
        {id && <Button title="Supprimer" color="#F00" onPress={_handleDelete} style={{flex: 1}} />}
      </View>
    </View>
  )
}

const styles = StyleSheet.create(theme => ({
  container: {
    flex: 1,
    justifyContent: "space-between",
    paddingHorizontal: 15,
    paddingVertical: 30
  },
  formGroup: {
    marginTop: 15
  },
  label: {
    fontSize: 20,
    color: theme.colors.text
  },
  actions: {
    flexDirection: "row",
  }
}));

export default ClientEdit;