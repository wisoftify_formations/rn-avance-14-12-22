import React from "react";
import {Text, View} from "react-native";

import Button from "../../uikit/Button";
import TextInput from "../../uikit/TextInput";

import useUiContext from "../../contexts/ui";

const Sandbox = ({title}) => {
  const [theme, setTheme] = useUiContext();

  return (
    <View>
      <Text style={{color: "red", fontSize: 42}}>hello world</Text>
      <Button title="ChangeTheme" onPress={() => setTheme(!theme)} />
      {
        theme ? <Text>Dark</Text> : <Text>Light</Text>
      }
    </View>
  )
}

export default Sandbox;