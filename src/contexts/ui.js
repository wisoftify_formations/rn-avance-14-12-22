import React, {createContext, useState, useContext} from "react";
import { useColorScheme } from 'react-native'; //light / dark

const UiContext = createContext();

const Provider = ({children}) => {
  const colorScheme = useColorScheme();
  console.log(colorScheme);
  const [theme, setTheme] = useState(colorScheme === 'dark' ? true : false);

  return (
    <UiContext.Provider value={[theme, setTheme]}>
      {children}
    </UiContext.Provider>
  )
}

const useUiContext = () => useContext(UiContext);

export {Provider};
export default useUiContext;