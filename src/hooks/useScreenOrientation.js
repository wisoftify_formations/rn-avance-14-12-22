//hook for get screen orientation on mobile

import {useState, useEffect} from "react";
import {Dimensions} from "react-native";

const useScreenOrientation = () => {
  const [screenOrientation, setScreenOrientation] = useState("portrait");

  useEffect(() => {
    //register to screeen orientation change
    Dimensions.addEventListener("change", () => {
      const {width, height} = Dimensions.get("window");
      if (width < height) {
        setScreenOrientation("portrait");
      } else {
        setScreenOrientation("landscape");
      }
    });
    return () => {
      //unregister to screeen orientation change
      Dimensions.removeEventListener("change", () => {});
    }
  }, []);

  return screenOrientation;
}

export default useScreenOrientation;