import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from "react-native";

const Fab = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={styles.text}>+</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    width: 65,
    height: 65,
    backgroundColor: "#0074cd",
    position: "absolute",
      bottom: 20,
      right: 20,
    borderRadius: 50
  },
  text: {
    fontSize: 48,
    color: "white",
  }
})

export default Fab;