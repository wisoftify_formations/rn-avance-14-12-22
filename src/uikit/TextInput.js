import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import * as PT from "prop-types";
import {useTheme} from "@react-navigation/native";

const MyTextInput = (props) => {
  const theme = useTheme();
  return <TextInput {...props} style={StyleSheet.compose(styles(theme).input, props.style ?? {})} placeholderTextColor={theme.colors.border} />
}

const styles = StyleSheet.create((theme) => ({
  input: {
    borderWidth: 1,
    borderColor: theme.colors.border,
    borderRadius: 5,
    backgroundColor: theme.colors.background,
    color: theme.colors.text,
  }
}))


MyTextInput.defaultProps = {//non recommandé
  value: "",
}

MyTextInput.propTypes = {
  onChangeText: PT.func.isRequired,// fonction appelée lors de la maj de la valeure
  value: PT.string,
  style: PT.object,
}

export default MyTextInput;