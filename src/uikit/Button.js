import React from "react";
import {Text, TouchableOpacity, StyleSheet} from "react-native";
import * as PT from "prop-types";
import {useTheme} from "@react-navigation/native";

const Button = ({
  title,
  onPress = () => {},
  color = "#0068b7",
  style = {}
}) => {
  const theme = useTheme();

  return (
    <TouchableOpacity onPress={onPress} style={StyleSheet.compose(styles(theme).container, {backgroundColor: color}, style)}>
      <Text style={styles(theme).text}>{title}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create((theme) => ({
  container: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  text: {
    color: "#fff"
  }
}))

Button.propTypes = {
  title: PT.string.isRequired,
  onPress: PT.func.isRequired,
  color: PT.string,
  style: PT.object
}

export default Button;