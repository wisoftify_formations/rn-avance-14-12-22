import React from 'react';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import useUiContext, { Provider } from './src/contexts/ui';

import Es6 from "./src/pages/Es6";
import Sandbox from './src/pages/Sandbox';
import Erp from "./src/pages/Erp";

const Tab = createBottomTabNavigator();

const App = () => {
  const [theme] = useUiContext();

  return (
    <NavigationContainer theme={theme ? DarkTheme : DefaultTheme}>
      <Tab.Navigator initialRouteName="Es6">
        <Tab.Screen name="Es6" component={Es6} />
        <Tab.Screen name="Sandbox" component={Sandbox} />
        <Tab.Screen name="Erp" component={Erp} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const AppWrapper = () => <Provider><App /></Provider>;
export default AppWrapper;